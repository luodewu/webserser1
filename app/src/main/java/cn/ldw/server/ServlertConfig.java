package cn.ldw.server;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import cn.ldw.servlet.AppRecomendDetailServlet;
import cn.ldw.servlet.AppServlet;
import cn.ldw.servlet.CategoryServlet;
import cn.ldw.servlet.CrossFireCategoriesServlet;
import cn.ldw.servlet.CrossFireNewsCFHDServlet;
import cn.ldw.servlet.CrossFireNewsHuodongServlet;
import cn.ldw.servlet.CrossFireNewsSaishiServlet;
import cn.ldw.servlet.CrossFireNewsShinianServlet;
import cn.ldw.servlet.CrossFireNewsZixunServlet;
import cn.ldw.servlet.DetailServlet;
import cn.ldw.servlet.DownloadServlet;
import cn.ldw.servlet.HomeInsertServlet;
import cn.ldw.servlet.GameServlet;
import cn.ldw.servlet.HomeServlet;
import cn.ldw.servlet.HotServlet;
import cn.ldw.servlet.ImageServlet;
import cn.ldw.servlet.RecommendServlet;
import cn.ldw.servlet.SubjectServlet;
import cn.ldw.servlet.TakeoutBusinessServlet;
import cn.ldw.servlet.TakeoutImgServlet;
import cn.ldw.servlet.TakeoutLoginServlet;
import cn.ldw.servlet.TakeoutOrderServlet;
import cn.ldw.servlet.TakeoutServlet;
import cn.ldw.servlet.UserServlet;

public class ServlertConfig {
	public static void config(ServletContextHandler handler) {
		handler.addServlet(new ServletHolder(new CategoryServlet()), "/category");
		handler.addServlet(new ServletHolder(new ImageServlet()), "/image");
		handler.addServlet(new ServletHolder(new RecommendServlet()), "/recommend");
		handler.addServlet(new ServletHolder(new SubjectServlet()), "/subject");
		handler.addServlet(new ServletHolder(new DetailServlet()), "/detail");
		handler.addServlet(new ServletHolder(new HomeServlet()), "/home");
		handler.addServlet(new ServletHolder(new AppServlet()), "/app");
		handler.addServlet(new ServletHolder(new GameServlet()), "/game");
		handler.addServlet(new ServletHolder(new DownloadServlet()), "/download");
		handler.addServlet(new ServletHolder(new UserServlet()), "/user");
		handler.addServlet(new ServletHolder(new HotServlet()), "/hot");
		handler.addServlet(new ServletHolder(new HomeInsertServlet()), "/homeinsert");
		handler.addServlet(new ServletHolder(new AppRecomendDetailServlet()), "/apprecommenddetail");
		handler.addServlet(new ServletHolder(new CrossFireCategoriesServlet()), "/categories");
		handler.addServlet(new ServletHolder(new CrossFireNewsZixunServlet()), "/zixun");
		handler.addServlet(new ServletHolder(new CrossFireNewsShinianServlet()), "/shinian");
		handler.addServlet(new ServletHolder(new CrossFireNewsCFHDServlet()), "/cfhd");
		handler.addServlet(new ServletHolder(new CrossFireNewsHuodongServlet()), "/huodong");
		handler.addServlet(new ServletHolder(new CrossFireNewsSaishiServlet()), "/saishi");
		handler.addServlet(new ServletHolder(new TakeoutServlet()), "/takeout");
		handler.addServlet(new ServletHolder(new TakeoutLoginServlet()), "/takelogin");
		handler.addServlet(new ServletHolder(new TakeoutOrderServlet()), "/takeorder");
		handler.addServlet(new ServletHolder(new TakeoutBusinessServlet()), "/takebusiness");
		handler.addServlet(new ServletHolder(new TakeoutBusinessImgServlet()), "/businessimg");
		handler.addServlet(new ServletHolder(new TakeoutImgServlet()), "/takeoutImg");
	}
}